package beans;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AttributeOfInquiryTest {
    private static final String STRING_NORMAL_TEST = "Test";
    private static final String STRING_EMPTY_TEST = "";

    @Test
    public void getNameTest() {
        AttributeOfInquiry attribute = new AttributeOfInquiry(STRING_NORMAL_TEST,STRING_NORMAL_TEST);
        assertEquals(STRING_NORMAL_TEST, attribute.getName());
    }

    @Test
    public void getValueTest() {
        AttributeOfInquiry attribute = new AttributeOfInquiry(STRING_NORMAL_TEST,STRING_NORMAL_TEST);
        assertEquals(STRING_NORMAL_TEST, attribute.getValue());
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorEmptyValueTest() {
        new AttributeOfInquiry(STRING_NORMAL_TEST, STRING_EMPTY_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorEmptyNameTest() {
        new AttributeOfInquiry(STRING_EMPTY_TEST, STRING_NORMAL_TEST );
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorNullValueTest() {
        new AttributeOfInquiry(STRING_NORMAL_TEST, null);
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorNullNameTest() {
        new AttributeOfInquiry(null, STRING_NORMAL_TEST );
    }
}
