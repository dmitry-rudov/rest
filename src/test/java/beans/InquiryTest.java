package beans;

import org.junit.Test;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InquiryTest {
    private static final int INT_ZERO_TEST = 0;
    private static final int INT_MAX_VALUE_TEST = Integer.MAX_VALUE;
    private static final int INT_MIN_VALUE_TEST = Integer.MIN_VALUE;

    private static final String STRING_NORMAL_TEST = "Test";
    private static final String STRING_EMPTY_TEST = "";

    private static final Date DATE_NORMAL_TEST = new Date(System.currentTimeMillis());
    private static final Topic TOPIC_NORMAL_TEST = new Topic(1, "Topic test");
    private static final List<AttributeOfInquiry> ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST =
                            Arrays.asList(new AttributeOfInquiry("Test name one", "Test value one"),
                                          new AttributeOfInquiry("Test name two", "Test value two"));

    //Tests getIdInquiry
    @Test
    public void getIdInquiryZeroForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_ZERO_TEST,
                                      STRING_NORMAL_TEST,
                                      STRING_NORMAL_TEST,
                                      DATE_NORMAL_TEST,
                                      TOPIC_NORMAL_TEST,
                                      ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(0, inquiry.getIdInquiry());
    }

    @Test
    public void getIdInquiryZeroForConstructorWithoutIdInquiryTest() {
        Inquiry inquiry = new Inquiry(STRING_NORMAL_TEST,
                                      STRING_NORMAL_TEST,
                                      DATE_NORMAL_TEST,
                                      TOPIC_NORMAL_TEST,
                                      ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(0, inquiry.getIdInquiry());
    }

    @Test
    public void getIdInquiryMaxValueForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MAX_VALUE_TEST,
                                      STRING_NORMAL_TEST,
                                      STRING_NORMAL_TEST,
                                      DATE_NORMAL_TEST,
                                      TOPIC_NORMAL_TEST,
                                      ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(Integer.MAX_VALUE, inquiry.getIdInquiry());
    }

    @Test
    public void getIdInquiryMinValueForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                                      STRING_NORMAL_TEST,
                                      STRING_NORMAL_TEST,
                                      DATE_NORMAL_TEST,
                                      TOPIC_NORMAL_TEST,
                                      ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(Integer.MIN_VALUE, inquiry.getIdInquiry());
    }

    //Tests getCustomers
    @Test
    public void getCustomerNameNormalForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals("Test", inquiry.getCustomerName());
    }

    @Test
    public void getCustomerNameNormalForConstructorWithoutIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals("Test", inquiry.getCustomerName());
    }

    @Test(expected=IllegalArgumentException.class)
    public void customerNameEmptyForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_EMPTY_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void customerNameEmptyForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_EMPTY_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void customerNameNullForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                null,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void customerNameNullForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                null,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    //Tests getDescription
    @Test
    public void getDescriptionNormalForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals("Test", inquiry.getDescription());
    }

    @Test
    public void getDescriptionNormalForConstructorWithoutIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals("Test", inquiry.getDescription());
    }

    @Test(expected=IllegalArgumentException.class)
    public void descriptionEmptyForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_EMPTY_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void descriptionEmptyForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_EMPTY_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void descriptionNullForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                null,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void descriptionNullForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                null,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    //Tests getDate
    @Test
    public void getDateNormalForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(DATE_NORMAL_TEST.toString(), inquiry.getCreateDate().toString());
    }

    @Test
    public void getDateNormalForConstructorWithoutIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(DATE_NORMAL_TEST.toString(), inquiry.getCreateDate().toString());
    }

    @Test(expected=IllegalArgumentException.class)
    public void dateNullForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                null,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void dateNullForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                null,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    //Tests getTopic
    @Test
    public void getTopicNormalForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(TOPIC_NORMAL_TEST.getId(), inquiry.getTopic().getId());
        assertEquals(TOPIC_NORMAL_TEST.getName(), inquiry.getTopic().getName());
    }

    @Test
    public void getTopicNormalForConstructorWithoutIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(TOPIC_NORMAL_TEST.getId(), inquiry.getTopic().getId());
        assertEquals(TOPIC_NORMAL_TEST.getName(), inquiry.getTopic().getName());
    }

    @Test(expected=IllegalArgumentException.class)
    public void topicNullForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                null,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void topicNullForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                null,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
    }

    //Tests getAttributes
    @Test
    public void getAttributesNormalForConstructorWithIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(2, inquiry.getAttributes().size());
        assertEquals("Test name one", inquiry.getAttributes().get(0).getName());
        assertEquals("Test value one", inquiry.getAttributes().get(0).getValue());
        assertEquals("Test name two", inquiry.getAttributes().get(1).getName());
        assertEquals("Test value two", inquiry.getAttributes().get(1).getValue());
    }

    @Test
    public void getAttributesNormalForConstructorWithoutIdInquiryTest() {
        Inquiry inquiry = new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                ATTRIBUTE_OF_INQUIRY_LIST_NORMAL_TEST);
        assertEquals(2, inquiry.getAttributes().size());
        assertEquals("Test name one", inquiry.getAttributes().get(0).getName());
        assertEquals("Test value one", inquiry.getAttributes().get(0).getValue());
        assertEquals("Test name two", inquiry.getAttributes().get(1).getName());
        assertEquals("Test value two", inquiry.getAttributes().get(1).getValue());
    }

    @Test(expected=IllegalArgumentException.class)
    public void attributesNullForConstructorWithIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                null);
    }

    @Test(expected=IllegalArgumentException.class)
    public void attributesNullForConstructorWithoutIdInquiryTest() {
        new Inquiry(INT_MIN_VALUE_TEST,
                STRING_NORMAL_TEST,
                STRING_NORMAL_TEST,
                DATE_NORMAL_TEST,
                TOPIC_NORMAL_TEST,
                null);
    }
}
