package beans;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TopicTest {
    private static final int INT_ZERO_TEST = 0;
    private static final int INT_MAX_VALUE_TEST = Integer.MAX_VALUE;
    private static final int INT_MIN_VALUE_TEST = Integer.MIN_VALUE;

    private static final String STRING_NORMAL_TEST = "Test";
    private static final String STRING_EMPTY_TEST = "";


    @Test
    public void getIdZeroTest() {
        Topic topic = new Topic(INT_ZERO_TEST, STRING_NORMAL_TEST);
        assertEquals(0, topic.getId());
    }

    @Test
    public void getIdMaxValueTest() {
        Topic topic = new Topic(INT_MAX_VALUE_TEST, STRING_NORMAL_TEST);
        assertEquals(Integer.MAX_VALUE, topic.getId());
    }

    @Test
    public void getIdMinValueTest() {
        Topic topic = new Topic(INT_MIN_VALUE_TEST, STRING_NORMAL_TEST);
        assertEquals(Integer.MIN_VALUE, topic.getId());
    }

    @Test
    public void getNameNormalTest() {
        Topic topic = new Topic(INT_ZERO_TEST, STRING_NORMAL_TEST);
        assertEquals(STRING_NORMAL_TEST, topic.getName());
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorEmptyTest() {
        new Topic(INT_ZERO_TEST, STRING_EMPTY_TEST);
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorNullTest() {
        new Topic(INT_ZERO_TEST, null);
    }
}
