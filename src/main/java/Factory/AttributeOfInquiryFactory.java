package Factory;

import DAO.DB.DBAttributeOfInquiryImpl;
import DAO.interfaces.IAttributeOfInquiryDAO;


public class AttributeOfInquiryFactory {
    public static IAttributeOfInquiryDAO getClassFromFactory() {
        return new DBAttributeOfInquiryImpl();
    }
}
