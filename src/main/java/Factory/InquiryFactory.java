package Factory;

import DAO.DB.DBInquiryImpl;
import DAO.interfaces.IInquiryDAO;

public class InquiryFactory {
    public static IInquiryDAO getClassFromFactory() {
        return new DBInquiryImpl();
    }
}
