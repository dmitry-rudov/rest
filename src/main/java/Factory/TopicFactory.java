package Factory;

import DAO.DB.DBTopicImpl;
import DAO.interfaces.ITopicDAO;

public class TopicFactory {
    public static ITopicDAO getClassFromFactory() {
        return new DBTopicImpl();
    }
}
