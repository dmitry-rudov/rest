package controller;

import DAO.interfaces.ITopicDAO;
import Factory.TopicFactory;
import beans.Topic;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TopicsController {

    @RequestMapping(method = RequestMethod.GET, value = "/topics")
    public List<Topic> getTopics() {
        ITopicDAO topicDAO = TopicFactory.getClassFromFactory();
        return topicDAO.getListTopic();
    }
}
