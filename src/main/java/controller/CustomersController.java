package controller;

import DAO.interfaces.IInquiryDAO;
import Factory.InquiryFactory;
import beans.Inquiry;
import controller.parser.RequestParser;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class CustomersController {

    @RequestMapping(method = RequestMethod.GET, value = "/customers/{customerName}/inquiries")
    public List<Inquiry> getInquiry(@PathVariable String customerName) {
        IInquiryDAO inquiryDAO = InquiryFactory.getClassFromFactory();
        return inquiryDAO.getListInquiry(customerName);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/customers/{customerName}/inquiries/{inquiryId}")
    public Inquiry getInquiry(@PathVariable String customerName, @PathVariable int inquiryId) {
        IInquiryDAO inquiryDAO = InquiryFactory.getClassFromFactory();
        return inquiryDAO.getInquiry(customerName, inquiryId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/customers/{customerName}/inquiries")
    public void postInquiry(@PathVariable String customerName,
                            @RequestBody Map<String, Object> mapRequest) {

        Inquiry inquiry = RequestParser.BuildInquiryOfRequest(customerName, mapRequest);

        IInquiryDAO inquiryDAO = InquiryFactory.getClassFromFactory();

        inquiryDAO.createInquiry(inquiry);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/customers/{customerName}/inquiries/{inquiryId}")
    public void putInquiry(@PathVariable String customerName,
                           @PathVariable int inquiryId,
                           @RequestBody Map<String, Object> mapRequest) {

        Inquiry inquiry = RequestParser.BuildInquiryOfRequest(inquiryId, customerName, mapRequest);

        IInquiryDAO inquiryDAO = InquiryFactory.getClassFromFactory();

        inquiryDAO.updateInquiry(inquiry);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/customers/{customerName}/inquiries/{inquiryId}")
    public void delInquiry(@PathVariable String customerName,
                           @PathVariable int inquiryId) {
        IInquiryDAO inquiryDAO = InquiryFactory.getClassFromFactory();
        inquiryDAO.deleteInquiry(customerName, inquiryId);
    }
}
