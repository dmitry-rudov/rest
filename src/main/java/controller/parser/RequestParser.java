package controller.parser;

import DAO.interfaces.ITopicDAO;
import Factory.TopicFactory;
import beans.AttributeOfInquiry;
import beans.Inquiry;
import constants.CommonsConstants;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RequestParser {
    public static Inquiry BuildInquiryOfRequest(int inquiryId, String customerName, Map<String,Object> map) {
        int idTopic = getInt(map, CommonsConstants.NAME_ID_TOPIC);
        String description = getString(map, CommonsConstants.NAME_DESCRIPTION);

        Date date = new Date(System.currentTimeMillis());
        ITopicDAO topicDAO = TopicFactory.getClassFromFactory();

        List<AttributeOfInquiry> attributes = getAttributeOfInquiries(map);

        return new Inquiry(inquiryId, customerName, description, date, topicDAO.getTopic(idTopic), attributes);
    }

    public static Inquiry BuildInquiryOfRequest(String customerName, Map<String,Object> map) {
        int idTopic = getInt(map, CommonsConstants.NAME_ID_TOPIC);
        String description = getString(map, CommonsConstants.NAME_DESCRIPTION);

        Date date = new Date(System.currentTimeMillis());
        ITopicDAO topicDAO = TopicFactory.getClassFromFactory();

        List<AttributeOfInquiry> attributes = getAttributeOfInquiries(map);

        return new Inquiry(customerName, description, date, topicDAO.getTopic(idTopic), attributes);
    }

    private static List<AttributeOfInquiry> getAttributeOfInquiries(Map<String, Object> map) {
        List<AttributeOfInquiry> attributes = new ArrayList<>();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            attributes.add(new AttributeOfInquiry(entry.getKey(),
                    (String)entry.getValue()));
        }
        return attributes;
    }

    private static String getString(Map<String, Object> map,String keyName) {
        String value = (String) map.get(keyName);
        map.remove(keyName);
        return value;
    }

    private static int getInt(Map<String, Object> map, String keyName) {
        int value = (Integer) map.get(keyName);
        map.remove(keyName);
        return value;
    }
}
