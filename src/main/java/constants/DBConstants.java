package constants;

public class DBConstants {
    public static final String DB_CLASS_NAME = "org.gjt.mm.mysql.Driver";
    public static final String DB_CONNECTION = "jdbc:mysql://localhost/omertex";
    public static final String DB_USER = "omertex";
    public static final String DB_PASSWORD = "omertex";

    public static final String QUERY_INSERT_INQUIRY = "INSERT " +
                                                      "INTO inquirys (id_topic, customer_name, description, create_date) " +
                                                      "VALUES(?, ?, ?, ?);";

    public static final String QUERY_SELECT_INQUIRY_ID = "SELECT * " +
                                                         "FROM inquirys " +
                                                         "WHERE customer_name = ? " +
                                                         "AND create_date = ? " +
                                                         "AND id_topic = ? " +
                                                         "AND description = ? " +
                                                         "AND id_inquiry = (SELECT MAX(id_inquiry) FROM inquirys);";

    public static final String QUERY_UPDATE_INQUIRY = "UPDATE inquirys " +
                                                      "SET id_topic = ?, description = ? " +
                                                      "WHERE id_inquiry = ?;";

    public static final String QUERY_DELETE_INQUIRY = "DELETE FROM inquirys " +
                                                      "WHERE id_inquiry = ? " +
                                                      "AND customer_name = ?;";

    public static final String QUERY_SELECT_INQUIRY_FOR_GET_INQUIRY = "SELECT * " +
                                                                       "FROM inquirys " +
                                                                       "WHERE id_inquiry = ? " +
                                                                       "AND customer_name = ?;";

    public static final String QUERY_SELECT_INQUIRY_FOR_GET_LIST_INQUIRY = "SELECT * " +
                                                                           "FROM inquirys " +
                                                                           "WHERE customer_name = ?;";

    public static final String QUERY_SELECT_TOPICS_FOR_GET_LIST_TOPIC = "SELECT * " +
                                                                        "FROM topics;";

    public static final String QUERY_SELECT_TOPICS_FOR_GET_TOPIC = "SELECT * " +
                                                                   "FROM topics " +
                                                                   "WHERE id_topic = ?;";

    public static final String QUERY_INSERT_ATTRIBUTES = "INSERT " +
                                                         "INTO attribute_of_inquiry (id_inquiry, name, value) " +
                                                         "VALUES(?, ?, ?);";

    public static final String QUERY_DELETE_ATTRIBUTES = "DELETE FROM attribute_of_inquiry " +
                                                         "WHERE id_inquiry = ?;";

    public static final String QUERY_SELECT_ATTRIBUTES_FOR_GET_LIST_ATTRIBUTE = "SELECT * " +
                                                                                "FROM attribute_of_inquiry " +
                                                                                "WHERE id_inquiry = ?;";

    public static final String ID_TOPIC_COLUMN_NAME = "id_topic";
    public static final String NAME_TOPIC_COLUMN_NAME = "name";

    public static final String ID_INQUIRY_COLUMN_NAME = "id_inquiry";
    public static final String CUSTOMER_NAME_INQUIRY_COLUMN_NAME = "customer_name";
    public static final String DESCRIPTION_INQUIRY_COLUMN_NAME = "description";
    public static final String CREATE_DATE_INQUIRY_COLUMN_NAME = "create_date";

    public static final String NAME_ATTRIBUTE_COLUMN_NAME = "name";
    public static final String VALUE_ATTRIBUTE_COLUMN_NAME = "value";
}
