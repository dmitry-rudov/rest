package constants;

public class ExceptionConstants {
    public static final String TOPIC_NAME_NULL_OR_EMPTY = "Field \"name\" can not be empty.";

    public static final String INQUIRY_CUSTOMER_NAME_NULL_OR_EMPTY = "Field \"customerName\" can not be empty.";
    public static final String INQUIRY_DESCRIPTION_NULL_OR_EMPTY = "Field \"description\" can not be empty.";
    public static final String INQUIRY_CRESTE_DATE_NULL = "Field \"currentDate\" can not be \"null\".";
    public static final String INQUIRY_TOPIC_NULL = "Field \"topic\" can not be \"null\".";
    public static final String INQUIRY_ATTRIBUTES_NULL = "Field \"attributes\" can not be \"null\".";

    public static final String ATTRIBUTE_OF_INQUIRY_NAME_NULL_OR_EMPTY = "Field \"name\" can not be empty.";
    public static final String ATTRIBUTE_OF_INQUIRY_VALUE_NULL_OR_EMPTY = "Field \"value\" can not be empty.";
}

