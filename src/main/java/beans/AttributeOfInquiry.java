package beans;

import constants.ExceptionConstants;

public class AttributeOfInquiry {
    private final String name;
    private final String value;

    private static void checkParameters(String name, String value) {
        if ((name == null) || (name.isEmpty())) {
            throw new IllegalArgumentException(ExceptionConstants.ATTRIBUTE_OF_INQUIRY_NAME_NULL_OR_EMPTY);
        }
        if ((value == null) || (value.isEmpty())) {
            throw new IllegalArgumentException(ExceptionConstants.ATTRIBUTE_OF_INQUIRY_VALUE_NULL_OR_EMPTY);
        }
    }

    public AttributeOfInquiry(String name, String value) {
        checkParameters(name, value);
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
