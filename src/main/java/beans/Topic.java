package beans;

import constants.ExceptionConstants;

public class Topic {
    private final int id;
    private final String name;

    private static void checkParameters(String name) {
        if ((name == null) || (name.isEmpty())) {
            throw new IllegalArgumentException(ExceptionConstants.TOPIC_NAME_NULL_OR_EMPTY);
        }
    }

    public Topic(int id, String name) {
        checkParameters(name);
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
