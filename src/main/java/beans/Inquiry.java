package beans;

import constants.ExceptionConstants;

import java.sql.Date;
import java.util.List;


public class Inquiry {

    private final int idInquiry;
    private final String customerName;
    private final String description;
    private final Date createDate;
    private final Topic topic;
    private final List<AttributeOfInquiry> attributes;

    private static void checkParameters(String customerName, String description, Date createDate,
                                        Topic topic, List<AttributeOfInquiry> attributes) {
        if ((customerName == null) || (customerName.isEmpty())) {
            throw new IllegalArgumentException(ExceptionConstants.INQUIRY_CUSTOMER_NAME_NULL_OR_EMPTY);
        }
        if ((description == null) || (description.isEmpty())) {
            throw new IllegalArgumentException(ExceptionConstants.INQUIRY_DESCRIPTION_NULL_OR_EMPTY);
        }
        if (createDate == null) {
            throw new IllegalArgumentException(ExceptionConstants.INQUIRY_CRESTE_DATE_NULL);
        }
        if (topic == null) {
            throw new IllegalArgumentException(ExceptionConstants.INQUIRY_TOPIC_NULL);
        }
        if (attributes == null) {
            throw new IllegalArgumentException(ExceptionConstants.INQUIRY_ATTRIBUTES_NULL);
        }
    }

    public Inquiry(String customerName, String description, Date createDate,
                   Topic topic, List<AttributeOfInquiry> attributes) {
        checkParameters(customerName, description, createDate, topic, attributes);
        idInquiry = 0;
        this.customerName = customerName;
        this.description = description;
        this.createDate = createDate;
        this.topic = topic;
        this.attributes = attributes;
    }


    public Inquiry(int idInquiry, String customerName, String description, Date createDate,
                   Topic topic, List<AttributeOfInquiry> attributes) {
        checkParameters(customerName, description, createDate, topic, attributes);
        this.idInquiry = idInquiry;
        this.customerName = customerName;
        this.description = description;
        this.createDate = createDate;
        this.topic = topic;
        this.attributes = attributes;
    }

    public int getIdInquiry() {
        return idInquiry;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getDescription() {
        return description;
    }

    public Topic getTopic() {
        return topic;
    }

    public List<AttributeOfInquiry> getAttributes() {
        return attributes;
    }
}
