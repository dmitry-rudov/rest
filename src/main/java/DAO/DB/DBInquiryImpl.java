package DAO.DB;

import DAO.DB.mappers.InquiryMapper;
import DAO.interfaces.IAttributeOfInquiryDAO;
import DAO.interfaces.IInquiryDAO;
import Factory.AttributeOfInquiryFactory;
import beans.AttributeOfInquiry;
import beans.Inquiry;
import constants.DBConstants;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Date;
import java.util.List;

public class DBInquiryImpl implements IInquiryDAO{
    private final JdbcTemplate jdbcTemplateObject = DBDataSource.getJDBCTemplate();

    @Override
    public synchronized void createInquiry(Inquiry inquiry) {
        int idInquiry;
        int idTopic = inquiry.getTopic().getId();
        List<AttributeOfInquiry> attributes = inquiry.getAttributes();
        String customerName = inquiry.getCustomerName();
        String description = inquiry.getDescription();
        Date createDate = inquiry.getCreateDate();
        IAttributeOfInquiryDAO attributeDAO = AttributeOfInquiryFactory.getClassFromFactory();

        jdbcTemplateObject.update(DBConstants.QUERY_INSERT_INQUIRY,
                idTopic, customerName, description, createDate);

        Inquiry inquiryForIDInquiry = jdbcTemplateObject.queryForObject(DBConstants.QUERY_SELECT_INQUIRY_ID,
                new Object[]{customerName, createDate, idTopic, description},
                new InquiryMapper());


        idInquiry = inquiryForIDInquiry.getIdInquiry();

        attributeDAO.createAttributes(idInquiry, attributes);
    }

    @Override
    public void updateInquiry(Inquiry inquiry) {
        int idInquiry = inquiry.getIdInquiry();
        List<AttributeOfInquiry> attributes = inquiry.getAttributes();
        IAttributeOfInquiryDAO attributeDAO = AttributeOfInquiryFactory.getClassFromFactory();

        jdbcTemplateObject.update(DBConstants.QUERY_UPDATE_INQUIRY,
                inquiry.getTopic().getId(), inquiry.getDescription(), idInquiry);
        attributeDAO.updateAttributes(idInquiry, attributes);
    }

    @Override
    public void deleteInquiry(String customerName, int idInquiry) {
        IAttributeOfInquiryDAO attributeDAO = AttributeOfInquiryFactory.getClassFromFactory();
        attributeDAO.deleteAttributes(idInquiry);
        jdbcTemplateObject.update(DBConstants.QUERY_DELETE_INQUIRY, idInquiry, customerName);
    }

    @Override
    public Inquiry getInquiry(String customerName, int idInquiry) {
        Inquiry inquiry = jdbcTemplateObject.queryForObject(DBConstants.QUERY_SELECT_INQUIRY_FOR_GET_INQUIRY,
                new Object[]{idInquiry, customerName},
                new InquiryMapper());
        return inquiry;
    }

    @Override
    public List<Inquiry> getListInquiry(String customerName) {
        List<Inquiry> inquirys = jdbcTemplateObject.query(DBConstants.QUERY_SELECT_INQUIRY_FOR_GET_LIST_INQUIRY,
                new Object[]{customerName}, new InquiryMapper());
        return inquirys;
    }
}
