package DAO.DB.mappers;

import beans.Topic;
import constants.DBConstants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class TopicMapper implements RowMapper<Topic> {
    @Override
    public Topic mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt(DBConstants.ID_TOPIC_COLUMN_NAME);
        String name = resultSet.getString(DBConstants.NAME_TOPIC_COLUMN_NAME);
        return new Topic(id, name);
    }
}
