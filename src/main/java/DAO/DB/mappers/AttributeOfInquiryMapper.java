package DAO.DB.mappers;

import beans.AttributeOfInquiry;
import constants.DBConstants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttributeOfInquiryMapper implements RowMapper<AttributeOfInquiry> {
    @Override
    public AttributeOfInquiry mapRow(ResultSet resultSet, int i) throws SQLException {
        String name = resultSet.getString(DBConstants.NAME_ATTRIBUTE_COLUMN_NAME);
        String value = resultSet.getString(DBConstants.VALUE_ATTRIBUTE_COLUMN_NAME);
        return new AttributeOfInquiry(name, value);
    }
}
