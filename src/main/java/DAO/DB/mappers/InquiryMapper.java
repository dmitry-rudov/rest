package DAO.DB.mappers;

import DAO.interfaces.IAttributeOfInquiryDAO;
import DAO.interfaces.ITopicDAO;
import Factory.AttributeOfInquiryFactory;
import Factory.TopicFactory;
import beans.Inquiry;
import constants.DBConstants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InquiryMapper implements RowMapper<Inquiry> {
    @Override
    public Inquiry mapRow(ResultSet resultSet, int i) throws SQLException {
        int idInquiry = resultSet.getInt(DBConstants.ID_INQUIRY_COLUMN_NAME);
        int idTopic = resultSet.getInt(DBConstants.ID_TOPIC_COLUMN_NAME);
        String customerName = resultSet.getString(DBConstants.CUSTOMER_NAME_INQUIRY_COLUMN_NAME);
        String description = resultSet.getString(DBConstants.DESCRIPTION_INQUIRY_COLUMN_NAME);
        Date createDate = resultSet.getDate(DBConstants.CREATE_DATE_INQUIRY_COLUMN_NAME);

        ITopicDAO topicDAO = TopicFactory.getClassFromFactory();
        IAttributeOfInquiryDAO attributesDAO = AttributeOfInquiryFactory.getClassFromFactory();

        return new Inquiry(idInquiry, customerName, description, createDate,
                topicDAO.getTopic(idTopic), attributesDAO.getAttributes(idInquiry));
    }
}
