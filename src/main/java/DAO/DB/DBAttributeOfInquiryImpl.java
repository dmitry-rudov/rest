package DAO.DB;

import DAO.DB.mappers.AttributeOfInquiryMapper;
import DAO.interfaces.IAttributeOfInquiryDAO;
import beans.AttributeOfInquiry;
import constants.DBConstants;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class DBAttributeOfInquiryImpl implements IAttributeOfInquiryDAO {
    private final JdbcTemplate jdbcTemplateObject = DBDataSource.getJDBCTemplate();

    @Override
    public void createAttributes(int idInquiry, List<AttributeOfInquiry> attributes) {
        for (AttributeOfInquiry attribute : attributes) {
            jdbcTemplateObject.update(DBConstants.QUERY_INSERT_ATTRIBUTES,
                    idInquiry, attribute.getName(), attribute.getValue());
        }
    }

    @Override
    public void updateAttributes(int idInquiry, List<AttributeOfInquiry> attributes) {
        deleteAttributes(idInquiry);
        createAttributes(idInquiry, attributes);
    }

    @Override
    public void deleteAttributes(int idInquiry) {
        jdbcTemplateObject.update(DBConstants.QUERY_DELETE_ATTRIBUTES, idInquiry);
    }

    @Override
    public List<AttributeOfInquiry> getAttributes(int idInquiry) {
        List<AttributeOfInquiry> attributes = jdbcTemplateObject.query(DBConstants.QUERY_SELECT_ATTRIBUTES_FOR_GET_LIST_ATTRIBUTE,
                new Object[]{idInquiry}, new AttributeOfInquiryMapper());
        return attributes;
    }
}
