package DAO.DB;

import DAO.DB.mappers.TopicMapper;
import DAO.interfaces.ITopicDAO;
import beans.Topic;
import constants.DBConstants;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class DBTopicImpl implements ITopicDAO {
    private final JdbcTemplate jdbcTemplateObject = DBDataSource.getJDBCTemplate();

    @Override
    public Topic getTopic(int idTopic) {
        Topic topic = jdbcTemplateObject.queryForObject(DBConstants.QUERY_SELECT_TOPICS_FOR_GET_TOPIC,
                new Object[]{idTopic}, new TopicMapper());
        return topic;
    }

    @Override
    public List<Topic> getListTopic() {
        List<Topic> topics = jdbcTemplateObject.query(DBConstants.QUERY_SELECT_TOPICS_FOR_GET_LIST_TOPIC,
                new TopicMapper());
        return topics;
    }
}
