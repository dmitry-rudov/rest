package DAO.DB;

import constants.DBConstants;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DBDataSource {
    private static DriverManagerDataSource dataSource;


    public static void setDataSource() {
        if (dataSource == null) {
            dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName(DBConstants.DB_CLASS_NAME);
            dataSource.setUrl(DBConstants.DB_CONNECTION);
            dataSource.setUsername(DBConstants.DB_USER);
            dataSource.setPassword(DBConstants.DB_PASSWORD);
        }
    }

    private DBDataSource() {
    }

    public static JdbcTemplate getJDBCTemplate() {
        setDataSource();
        return new JdbcTemplate(dataSource);
    }
}
