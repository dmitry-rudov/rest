package DAO.interfaces;

import beans.AttributeOfInquiry;

import java.util.List;

public interface IAttributeOfInquiryDAO {
    public void createAttributes(int idInquiry, List<AttributeOfInquiry> attributes);
    public void updateAttributes(int idInquiry, List<AttributeOfInquiry> attributes);
    public void deleteAttributes(int idInquiry);
    public List<AttributeOfInquiry> getAttributes(int idInquiry);
}
