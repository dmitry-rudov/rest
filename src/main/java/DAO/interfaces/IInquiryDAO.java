package DAO.interfaces;

import beans.Inquiry;

import java.util.List;

public interface IInquiryDAO {
    public void createInquiry(Inquiry inquiry);
    public void updateInquiry(Inquiry inquiry);
    public void deleteInquiry(String customerName, int idInquiry);
    public Inquiry getInquiry(String customerName, int idInquiry);
    public List<Inquiry> getListInquiry(String customerName);
}
