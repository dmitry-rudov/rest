package DAO.interfaces;

import beans.Topic;

import java.util.List;

public interface ITopicDAO {
    public List<Topic> getListTopic();
    public Topic getTopic(int idTopic);
}
